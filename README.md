# Angular OAuth Website
## Overview
This is a sample Angular OAuth client application.

This project was generated using the [Angular CLI], currently on version 1.3.2

## Software Development Life Cycle (SDLC)
This project uses the [GitFlow] workflow and supports continuous integration using [GitLab CI] for build automation and [Octopus Deploy] for deployment automation.

## How to get started
### Running the supporting Service

The website relies on the [WebAPI OAuth Service], please refer to the the [WebAPI OAuth Service ReadMe] for further details on running the supporting project.

### Prerequisites
- [NodeJS] - The version used as of publishing is [node-v8.1.4-x64]
  - In a command window enter `node -v` to check your installed version
- [Angular CLI] - The Angular CLI is used to host, build and generate components for the Angular projects
  - In a command shell enter `npm install -g @angular/cli`
- [Compodoc] - Component documentation tool for Angular CLI 
  - In a command shell enter `npm install -g @compodoc/compodoc`
- [VS Code] - A light weight code editor with intellisense
- [Debugger for Chrome]
  - Open VS Code, press **Ctrl+P** then enter `ext install debugger-for-chrome`

### Recommended Tools / Plugins for VS Code

Launch VS Code 'Quick Open' (Ctrl+P), enter the following commands and press enter.
- [Markdown PDF]
  - `ext install markdown-pdf`
- [Auto-Open Markdown Preview]
  - `ext install vscode-auto-open-markdown-preview`

### Quick Start Scripts

Scripts have been included to assist with **building, debugging and deploying** the application.
- `scripts\Debug.ps1` - Starts the website project in debug local configuration (Accessible via `localhost` only)
- `scripts\Debug.External.ps1` - Starts the project in external debug configuration (Accessible via `angular-oauth.examples.spearone.net`) - you will have to setup the hostname appropriately to point to your instance.
- `scripts\Build.ps1` - Builds the project (in Production mode) using the [Angular CLI]
- `scripts\Clean.ps1` - Removes all generated artefacts produced by the build.
- `scripts\GenerateDocument.ps1` - Generates documentation for the application using [Compodoc]
- `scripts\Pack.ps1` - Packages the Website for deployment using [Octo.exe]

### Running the Website (Manually)

1. Open a command prompt in the projects root directory.
2. Run the following commands:
   - `npm install`
   - `ng serve`
3. Navigate to <http://localhost:4200> in your prefered browser.

Note: Once you run the `ng serve` command any changes you make to the website will be automatically recompiled on the fly.

### Running Tests

#### Unit Tests
Run `ng test` to execute the *unit tests* via [Karma].

#### End-to-end Tests
Run `ng e2e` to execute the *end-to-end tests* via [Protractor].
Before running the tests make sure you are serving the app via `ng serve`.

## Useful Commands

#### NodeJS / NPM
- `node -v` - Gets the version of NodeJS
- `npm -v` - Gets the version of the Node Package Manager
- `npm install` - Installs packages based on a packages.json file

#### AngularCLI
All of the following commands are part of the [Angular CLI]
- `ng serve` - hosts the project (The AngularCLI uses the [WebPack] development server internally)
- `ng build` - builds the Angular project
- `ng test` - run unit tests
- `ng e2e` - runs end to end tests

#### Compodoc
- `compodoc -p tsconfig.json --theme postmark --hideGenerator` - Generates documentation for the Angular project using **compodoc** with the *postmark* theme

## Authors
- Matthew Marlin 


[Octopus Deploy]: http://octopus.ocm.absolutesys.com
[GitLab CI]: https://gitlab.com/matthewamarlin/Angular.OAuth/pipelines
[WebAPI OAuth Service]: https://gitlab.com/matthewamarlin/WebApi.OAuth
[WebAPI OAuth Service ReadMe]: https://gitlab.com/matthewamarlin/WebApi.OAuth/blob/master/ReadMe.md
[Angular CLI]: https://cli.angular.io
[NodeJS]: https://nodejs.org/en
[VS Code]: https://code.visualstudio.com
[node-v8.1.4-x64]: https://nodejs.org/dist/v8.1.4/node-v8.1.4-x64.msi
[NPM]: https://www.npmjs.com/
[Angular]: https://angular.io
[LESS]: http://lesscss.org/
[TypeScript]: https://www.typescriptlang.org
[NPM Task Runner]: https://marketplace.visualstudio.com/items?itemName=MadsKristensen.NPMTaskRunner
[Open in VS Code]: https://marketplace.visualstudio.com/items?itemName=MadsKristensen.OpeninVisualStudioCode
[Compodoc]: https://www.npmjs.com/package/@compodoc/compodoc
[Markdown PDF]: https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf
[Auto-Open Markdown Preview]: https://marketplace.visualstudio.com/items?itemName=hnw.vscode-auto-open-markdown-preview
[Debugger for Chrome]: https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome
[GitFlow]: https://datasift.github.io/gitflow/IntroducingGitFlow.html
[Octo.exe]: https://octopus.com/docs/api-and-integration/octo.exe-command-line
[Protractor]: http://www.protractortest.org/
[Karma]: https://karma-runner.github.io
