Push-Location
Set-Location "$PSScriptRoot\..\"

<#call#> & npm install --loglevel=error
<#call#> & ng build --prod

Copy-Item ".\web.config" ".\dist\web.config"

Pop-Location
