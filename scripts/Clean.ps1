Push-Location
Set-Location "$PSScriptRoot\..\"

Remove-Item dist -Force -Recurse
Remove-Item node_modules -Force -Recurse
Remove-Item packed -Force -Recurse

Pop-Location
