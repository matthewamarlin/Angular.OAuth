param([string] $version = "0.0.0-dev")

$AppConfigFile = "./assets/config/app-config.json";
$TranformedAppConfigFile = "assets/config/app-config.release.json";

Push-Location
Set-Location "$PSScriptRoot/../dist"

# Transform Configuration for octopus
Move-Item $TranformedAppConfigFile $AppConfigFile -Force
(Get-Content $AppConfigFile).replace('#{Version}', $version) | Set-Content $AppConfigFile

New-Item -ItemType Directory -Force -Path ../packed/
octo pack --id="SpearOne.Examples.OAuth.Website" --format=Zip --outFolder=../packed/ --version=$version

Pop-Location
