﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { APP_INITIALIZER } from '@angular/core';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';

import 'rxjs/add/operator/map';

import { NavHeaderComponent } from './navheader/navheader.component';
import { MenuComponent } from './menu/menu.component';

import { AuthLayoutComponent, AppLayoutComponent } from './layouts/index';
//import { ChatComponent } from './chat/index';
import { GeoLocationService, TermsOfServiceComponent } from './core/index'; //SignalrWindow, ChatService
import { AuthHttp, AuthHttpModule, AuthHttpConfig, JwtHelper } from './authHttp/index'
import { ConfigService, AuthService, AuthGuard, Logger, ConsoleLogger, IFrameResizerDirective } from './core/index'

/* Helper functions for configuring globally availbable modules */

export function authHttpServiceFactory(http: Http, options: RequestOptions, authService: AuthService, configService: ConfigService) {
  return new AuthHttp(new AuthHttpConfig({
    tokenName: configService.get.auth.tokenStorageName,
    tokenGetter: (() => {
      return authService.getAccessToken().toPromise();
    }),
		globalHeaders: [{'Content-Type':'application/json'}],
	}), http, options);
}

export function appInitializer(configService: ConfigService) {
  return () => new Promise((resolve) => {

    if(environment.production){
        console.log("Production mode enabled!");
    }

    configService.load(environment.configFile).subscribe(() => {
        resolve();
    });
  });
}

/**
 * Detects the origin URI for the client application using a common Javascript polyfill from modernizr.
 * Link: https://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer/
 */
export function originFactory() {
    let wl = window.location;
    if (wl.origin) {
      return wl.origin;
    }
    let polyfill = wl.protocol + '//' + wl.hostname + (wl.port ? ':' + wl.port : '');
    return polyfill;
}

// const powerBIConfig: ReportsListServiceConfig = {
//         WebAPIServiceUrl: 'http://localhost:4200/api/PowerBi'
// };

/* Module Definition */
@NgModule({
  declarations: [
    AppComponent,
    AppLayoutComponent,
    AuthLayoutComponent,
    MenuComponent,
    NavHeaderComponent,
    //ChatComponent,
    TermsOfServiceComponent,
    IFrameResizerDirective
  ],
  imports: [
    FormsModule,
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    //AuthHttpModule.forRoot(authConfig),
  ],
  providers: [
    ConfigService,
    {
        provide: APP_INITIALIZER,
        useFactory: appInitializer,
        deps: [ConfigService],
        multi: true
    },
    ConsoleLogger,
    //ChatService,
    GeoLocationService,
    AuthGuard,
    AuthService,
    JwtHelper,
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions, AuthService, ConfigService]
    },
    //{ provide: SignalrWindow, useValue: window },
    { provide: 'ORIGIN_URL', useFactory: originFactory },
    { provide: Logger, useExisting: ConsoleLogger}
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
