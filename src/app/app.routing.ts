import { Routes } from '@angular/router';

import { AuthLayoutComponent, AppLayoutComponent } from './layouts/index';
import { AuthGuard, TermsOfServiceComponent } from './core/index'

export const AppRoutes: Routes = [{
    path: '',
    component: AppLayoutComponent,
    children: [{
        path: '',
        redirectTo: '/dashboard',
        pathMatch: "full",
        canActivate: [AuthGuard]
    },
    {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'mail',
        loadChildren: './mail/mail.module#MailModule',
        canActivate: [AuthGuard]
    }]
}, {
    path: '',
    component: AuthLayoutComponent,
    children: [{
        path: 'auth',
        loadChildren: './auth/auth.module#AuthenticationModule'
    }, {
        path: 'error',
        loadChildren: './error/error.module#ErrorModule'
    }, {
        path: 'termsofservice',
        component: TermsOfServiceComponent
    }]
}, {
    path: '**',
    redirectTo: 'error/404'
}];