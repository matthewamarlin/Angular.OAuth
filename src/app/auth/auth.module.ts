import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule,
        ReactiveFormsModule} from '@angular/forms';
import {HttpModule, Http} from '@angular/http'


import {AuthenticationRoutes} from './auth.routing'
import {SigninComponent,
        CreateAccountComponent,
        ConfirmAccountComponent,
        ForgotPasswordComponent,
        LockedOutComponent,
        ExternalLoginComponent,
        ResetPasswordComponent,
        ShowHideInputComponent} from './index';
import {TitledHrComponent} from '../shared/index'
import {AuthService} from '../core/index'

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(AuthenticationRoutes),
      FormsModule,
      ReactiveFormsModule,
      HttpModule,
  ],
  declarations: [
      SigninComponent,
      CreateAccountComponent,
      ConfirmAccountComponent,
      ForgotPasswordComponent,
      LockedOutComponent,
      TitledHrComponent,
      ExternalLoginComponent,
      ResetPasswordComponent,
      ShowHideInputComponent
  ],
  providers: [

  ]
})

export class AuthenticationModule {}
