import { Routes } from '@angular/router';

import { SigninComponent, 
  CreateAccountComponent, 
  ForgotPasswordComponent,
  LockedOutComponent,
  ExternalLoginComponent,
  ResetPasswordComponent,
  ConfirmAccountComponent } from './index';

export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [{
      path: '',
      redirectTo: 'signin'
    },{
      path: 'signin',
      component: SigninComponent
    }, {
      path: 'create',
      component: CreateAccountComponent
    }, {
      path: 'forgot',
      component: ForgotPasswordComponent
    }, {
      path: 'locked',
      component: LockedOutComponent
    }, {
      path: 'external',
      component: ExternalLoginComponent
    }, {
      path: 'reset',
      component: ResetPasswordComponent
    }, {
      path: 'confirm',
      component: ConfirmAccountComponent
    }]
  }
];
