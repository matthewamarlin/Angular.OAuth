import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Http, RequestOptions, RequestOptionsArgs } from '@angular/http' 
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'

import { Observable } from 'rxjs/Observable'
import swal from 'sweetalert2'

import { AuthService } from '../../core/index'

declare let $: any;

@Component({
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.less']
})
export class ConfirmAccountComponent implements OnInit, AfterViewInit {

  message: string;
  id: string;
  token: string;
  state: ConfirmAccountComponentState = ConfirmAccountComponentState.Loading;

  public ConfirmAccountComponentState = ConfirmAccountComponentState; //Make the enum accessible to the view

  constructor(private router: Router, private authService: AuthService, private activatedRoute: ActivatedRoute) {  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
        this.id = params['id'];
        this.token = params['token'];
    });
  }

  ngAfterViewInit(){
    var confirmationData = {'id': this.id, 'token': this.token };
    this.authService.confirmAccount(confirmationData).catch((error, caught) => {
      this.message = error;
      this.state = ConfirmAccountComponentState.Error;
      throw error;
    }).subscribe((message: string) => {
      this.message = message
      this.state = ConfirmAccountComponentState.Success;
    })
  }
}

export enum ConfirmAccountComponentState {
  "Error" = -1,
  "Loading" = 0,
  "Success" = 1
}