import { Http, RequestOptions, RequestMethod } from '@angular/http'
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

import { AuthService } from '../../core/index'
import { PasswordValidator } from '../../shared/index'

//TODO Does this break tree shaking?
declare let $: any;

@Component({
  //selector: 'auth-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.less']
})
export class CreateAccountComponent implements OnInit {

  public form: FormGroup;
  public posting: boolean = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private http: Http, private authService: AuthService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([
        Validators.required,
        Validators.minLength(6),
        PasswordValidator.uppercaseCharacterRule(1),
        PasswordValidator.lowercaseCharacterRule(1),
        PasswordValidator.digitCharacterRule(1),
        PasswordValidator.specialCharacterRule(1)
      ])],
      confirmPassword: [null, Validators.compose([Validators.required])],
      lastname: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      firstname: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      termsAndConditions: [false, Validators.compose([Validators.pattern('true')])],
    }, {
        validator: Validators.compose([ PasswordValidator.mismatchedPasswords()])
      });

    //TODO Causing problems with validation, underlying input form element is not toggled

    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    // $('input[type="checkbox"]').iCheck({
    //   checkboxClass: 'icheckbox_flat-blue',
    //   radioClass: 'iradio_flat-blue'
    // });
  }

  onSubmit() {

    this.posting = true;

    let registrationData = {
      email: this.form.get('email').value,
      firstname: this.form.get('firstname').value,
      lastname: this.form.get('lastname').value,
      password: this.form.get('password').value,
      confirmPassword: this.form.get('confirmPassword').value
    };

    this.authService.createAccount(registrationData).do(() => {
      this.posting = false;
    }).subscribe(response => this.handleSuccess(response), err => this.handleError(err));
  }

  private handleSuccess(response) {
    swal("Success", "Your account has been created, please check your email inbox to activate your account ", "success").then(() => {
      this.router.navigate(['/auth/signin']);
    })
  }

  private handleError(error) {
    let errors = [];
    for (let key in error) {
      for (let i = 0; i < error[key].length; i++) {
        errors.push(`<li>${error[key][i]}</li>`);
      }
    }

    swal({
      title: 'Server Rejected Registration',
      type: 'error',
      html: `
      <div class="text-left">
        <ul>
          ${errors.join('\r\n')}
        </ul>
      <div>
      `,
      showCloseButton: true,
    })
  }
}