import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IExternalAuthData } from '../../core/index'

declare let window: any;
declare let $: any;

@Component({
  // selector: 'external-auth-complete',
  template: `
<div class="content text-center">
  <span class="message">
    {{message}}
  </span>
</div>
  `,
  styles: [ ".message { width: 100% }" ]
})
export class ExternalLoginComponent implements OnInit {
  
  message: string;
  ngOnInit(): void {
    console.log("ExternalComponent OnInit")
      
    this.message = "You are being redirected"
      
    $(document).ready(() => {  
        opener.ExternalAuthCompleted(this.getQueryString())
        window.close();
    });
  }

  private getQueryString(): any {
      if (window.location.hash.indexOf("#") === 0) {
          return this.parseQueryString(window.location.hash.substr(1));
      } else {
          return {};
      }
  };

  private parseQueryString(queryString) {
      let data = {},
          pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;

      if (queryString === null) {
          return data;
      }

      pairs = queryString.split("&");

      for (let i = 0; i < pairs.length; i++) {
          pair = pairs[i];
          separatorIndex = pair.indexOf("=");

          if (separatorIndex === -1) {
              escapedKey = pair;
              escapedValue = null;
          } else {
              escapedKey = pair.substr(0, separatorIndex);
              escapedValue = pair.substr(separatorIndex + 1);
          }

          key = decodeURIComponent(escapedKey);
          value = decodeURIComponent(escapedValue);

          data[key] = value;
      }

      return data;
  }
}