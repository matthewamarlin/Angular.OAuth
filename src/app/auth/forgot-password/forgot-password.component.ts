import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { AuthService } from '../../core/index'
import swal from 'sweetalert2'

declare let $: any;

@Component({
  //selector: 'auth-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less']
})
export class ForgotPasswordComponent implements OnInit {

  public form: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      email: [null , Validators.compose ( [ Validators.required, Validators.email ] )]
    } );
  }

  onSubmit() {
    let email = this.form.get('email').value;
    this.authService.forgotPassword(email).catch((error) => {
        swal("", error, "error")
        throw error;
    }).subscribe(() => {
      swal("", "Password reset email sent, please check your inbox for further instructions", "success").then(() => {
        this.router.navigate(['/auth/signin'])
      })
    });
  }
}