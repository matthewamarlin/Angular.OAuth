import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  //selector: 'auth-locked-out',
  templateUrl: './locked-out.component.html',
  styleUrls: ['./locked-out.component.less']
})
export class LockedOutComponent {

  username: string = "Matthew Marlin"

  constructor(private router: Router) {}

  onSubmit() {
    this.router.navigate ( [ '/' ] );
  }
}
