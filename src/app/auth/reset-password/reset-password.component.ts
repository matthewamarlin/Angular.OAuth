import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { AuthService } from '../../core/index'
import { PasswordValidator } from '../../shared/index'

import swal from 'sweetalert2'

declare let $: any;

@Component({
  //selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

  public form: FormGroup;

  id: string;
  token: string;

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private activatedRoute: ActivatedRoute) {
    this.form = this.fb.group ( {
      password: [null, Validators.compose([ 
        Validators.required,
        Validators.minLength(6),
        PasswordValidator.uppercaseCharacterRule(1),
        PasswordValidator.lowercaseCharacterRule(1),
        PasswordValidator.digitCharacterRule(1),
        PasswordValidator.specialCharacterRule(1)
      ])],
      confirmPassword: [null , Validators.compose ( [ Validators.required ] )],
    } , {
      validator: Validators.compose( [ PasswordValidator.mismatchedPasswords() ] )
    } );
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
        this.id = params['id'];
        this.token = params['token'];
    });
  }

  onSubmit() {
    let password = this.form.get('password').value;
    let confirmPassword = this.form.get('confirmPassword').value;
    
    this.authService.resetPassword({
      'id': this.id,
      'token': this.token,
      'password': password,
      'confirmPassword': confirmPassword
    }).catch((error) => {
      
      if(error instanceof Array)
      {
        var ul = document.createElement('ul');
        for(let key in error){
          if (error.hasOwnProperty(key)){
            var li = document.createElement('li')
            li.appendChild(document.createTextNode(error[key]));
            ul.appendChild(li);
          }
        }
        swal("", ul.outerHTML, "error")
      }
      else
      {
        swal("", error, "error")
      }
      
      throw error;
    }).subscribe(()=>{
      swal("", "You have succesfully changed your password, and will be redirected back to login", "success").then(() => {
        this.router.navigate(['/auth/signin']);
      })
    })

  }
}