import {Component, ContentChild} from '@angular/core';

@Component({ selector: 'show-hide-input',
            templateUrl: './show-hide-input.component.html',
            styleUrls: ['./show-hide-input.component.less']
})
export class ShowHideInputComponent 
{
    public show = false;
    
    @ContentChild('showhideinput') input;
    
    constructor(){}
   
    toggleShow()
    {
        this.show = !this.show;
        
        if (this.show){
            this.input.nativeElement.type='text';
        }
        else {
            this.input.nativeElement.type='password';
        }
    }
}