import { NgZone, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2'

import { AuthService, IExternalAuthData, ISignInModel, ConfigService } from '../../core/index';
import { TitledHrComponent } from '../../shared/index'
import { ShowHideInputComponent } from '../show-hide-input/show-hide-input.component'

declare let $: any;
declare let screen: any;

@Component({
  selector: 'auth-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.less']
})
export class SigninComponent implements OnInit {

  //private oAuthWindow: any;

  public form: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService, private configService: ConfigService, private zone: NgZone)
  { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required])],
      rememberMe: [null]
    });

    // TODO Validation issues?

    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    // $('input[type="checkbox"]').iCheck({
    //   checkboxClass: 'icheckbox_flat-blue',
    //   radioClass: 'iradio_flat-blue'
    // });
  }

  onSubmit() {
    this.login();
  }

  login() {
    let loginData: ISignInModel = {
      email: this.form.get('email').value,
      password: this.form.get('password').value,
      rememberMe: this.form.get('rememberMe').value
    };
    this.authService.signIn(loginData).subscribe(resp => this.loginSuccess(), err => this.loginError(err));
  }

  loginSuccess() {
    this.router.navigate(['/']);
  }

  loginError(error: string) {
    swal({
      type: "error",
      text: error
    });
  }

  externalLogin(provider: string) {

    let redirectUri = location.protocol + '//' + location.host + '/auth/external';

    let externalProviderUrl = this.configService.get.serviceUrl + "/api/Account/ExternalLogin?provider=" + provider
      + "&response_type=token&client_id=" + this.configService.get.auth.clientId
      + "&redirect_uri=" + redirectUri;


    (<any>window).ExternalAuthCompleted = (data) => this.externalAuthCompleted(data);
    //HACK: 'any' used here to allow us to define our callback
    //this.oAuthWindow = this.popupCenter(externalProviderUrl, "Authenticate Account", 600,750);
    //this.oAuthWindow.ExternalAuthCompleted = this.externalAuthCompleted;
    let oAuthWindow: any = this.popupCenter(externalProviderUrl, "Authenticate Account", 600, 750);
    //oAuthWindow.ExternalAuthCompleted = (externalData) => this.externalAuthCompleted(externalData);
    //this.oAuthWindow = oAuthWindow;
  }

  private popupCenter(url, title, w, h): Window {

    // Fixes dual-screen position                         Most browsers      Firefox
    let dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    let dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    let left = ((width / 2) - (w / 2)) + dualScreenLeft;
    let top = ((height / 2) - (h / 2)) + dualScreenTop;
    let newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
      newWindow.focus();
    }

    return newWindow;
  }

  private externalAuthCompleted(externalData) {

    // This Zone.run() usage is required because the callback is coming from the external oAuthWindow.
    // This fixes an issue where the LifeCycle (ngOninit, ngAfterViewInit, etc..) where not being fired correctly.
    this.zone.run(() => {
      let hasLocalAccount = JSON.parse(externalData.haslocalaccount.toLowerCase());

      let externalAuthData: IExternalAuthData = {
        externalAccessToken: externalData.external_access_token,
        userName: externalData.external_user_name,
        emailAddress: externalData.external_email_address,
        hasLocalAccount: hasLocalAccount,
        provider: externalData.provider
      };

      if (hasLocalAccount) {
        //Obtain access token and redirect
        this.authService.externalSignIn(externalAuthData).subscribe(response => {
          //this.oAuthWindow.close();
          this.router.navigate(['/']);
        });
      }
      else {
        this.authService.registerExternalSignIn(externalAuthData).subscribe(response => {
          //this.oAuthWindow.close();
          this.router.navigate(['/']);
        }); //TODO Handle Error
      }
    });
  }
}
