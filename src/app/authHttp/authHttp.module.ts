import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthHttpConfig } from './authHttp.service';

// Forked from: https://github.com/auth0/angular2-jwt

/**
 * The AuthHttp module provides the following:
 *  - An http service that allows for explicitly authentication requests using JWT tokens which are automatically added to the header.
 *  - A JwtHelper class that assists with decoding and dealing with JWT tokens
 */
// @NgModule({
//   providers: [
//     {
//       provide: AuthHttp,
//       useFactory: authHttpServiceFactory,
//       deps: [Http, RequestOptions]
//     }
//   ]
// })
@NgModule()
export class AuthHttpModule {
  constructor(@Optional() @SkipSelf() parentModule: AuthHttpModule) {
    if (parentModule) {
      throw new Error(
          'AuthHttpModule is already loaded. Import it in the AppModule only');
    }
  }

  /**
   * Creates an instance of the Module with a Provider for the Configuration Options
   * @param config The AuthHttpConfig options
   */
  static forRoot(config: AuthHttpConfig): ModuleWithProviders {
    return {
      ngModule: AuthHttpModule,
      providers: [
        AuthHttp,
        { provide: AuthHttpConfig, useValue: config }
      ]
    };
  }
}

/**
 *  A service factory (alternative to forRoot) that returns an instance of the AuthHttp given the different options
 * @param http The angular http service 
 * @param options The default RequestOptions used for each request.
 */
// export function authHttpServiceFactory(http: Http, options: RequestOptions) {
//   return new AuthHttp(new AuthHttpConfig({
//     tokenName: 'token',
//     tokenGetter: (() => {
//       return sessionStorage.getItem('token')
//     }),
// 		globalHeaders: [{'Content-Type':'application/json'}],
// 	}), http, options);
// }

// export const AUTH_PROVIDERS: Provider[] = [
//   {
//     provide: AuthHttp,
//     deps: [Http, RequestOptions],
//     useFactory: (http: Http, options: RequestOptions) => {
//       return new AuthHttp(new AuthHttpConfig(), http, options);
//     }
//   }
// ];

// export function provideAuth(config?: IAuthConfigOptional): Provider[] {
//   return [
//     {
//       provide: AuthHttp,
//       deps: [Http, RequestOptions],
//       useFactory: (http: Http, options: RequestOptions) => {
//         return new AuthHttp(new AuthHttpConfig(config), http, options);
//       }
//     }
//   ];
// }
