// import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
// import { Http, Response } from "@angular/http";
// import { ChatService, IChatMessage, ConfigService } from "../core/index";

// @Component({
//     selector: 'chat',
//     templateUrl: './chat.component.html',
//     styleUrls: ['./chat.component.less']
// })
// export class ChatComponent implements OnInit {
//     messages: Array<IChatMessage> = [{
//         sender: "", body: "Chat Connected!"
//     }]

//     clientMessage: string;

//     constructor(
//         private changeDetectorRef: ChangeDetectorRef,
//         private http: Http,
//         private chatService: ChatService,

//         private configService: ConfigService
//     ) {

//     }

//     ngOnInit() {

//         this.chatService.messageReceived$.subscribe(
//             (message: IChatMessage) => {
//                 console.log(message.body);
//                 this.messages.push(message);
//                 this.changeDetectorRef.detectChanges();
//             },
//             (error: any) => {
//                 console.warn("Error subscribing to messageReceived$ Event: ", error);
//             }
//         );

//         this.chatService.start();
//     }

//     sendMessage(){
//         this.chatService.send("Web Site", this.clientMessage);
//         this.clientMessage = "";
//     }
// }
