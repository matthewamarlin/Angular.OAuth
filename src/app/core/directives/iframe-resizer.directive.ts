import { AfterViewInit, Directive, ElementRef, OnDestroy, Input } from '@angular/core';
import * as resizer from 'iframe-resizer';

@Directive({
    selector: 'iframe.resizer'
})
export class IFrameResizerDirective implements AfterViewInit, OnDestroy {
    component: any;
    constructor(public element: ElementRef) {
        console.log("iframe-resizer started")
    }

    ngAfterViewInit() {
        
        console.log("iframe-resizer started ngAfterViewInit")
        this.component = resizer.iframeResizer({
            checkOrigin: false,
            heightCalculationMethod: 'documentElementOffset',
            log: false
        }, this.element.nativeElement);
    }

    ngOnDestroy(): void {
        console.log("iframe-resizer started ngOnDestroy")
        if (this.component && this.component.iFrameResizer) {
            this.component.iFrameResizer.close();
        }
    }
}