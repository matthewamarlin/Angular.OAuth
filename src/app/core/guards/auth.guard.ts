import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs'

import { AuthService } from '../services/auth.service'
import { Logger } from '../services/logging.service'
import { LogLevel } from '../models/logging-levels'
 
@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthService, private log: Logger) { }
 
    canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<boolean> {
        return this.authService.isAuthenticated().do(loggedIn => {
            if(!loggedIn){
                this.log.write(LogLevel.Debug, `User is not authenticated`)
                this.router.navigate(['/auth/signin']);
            }
            else{
                this.log.write(LogLevel.Debug, `User is authenticated`)
            }
        });
    }
}