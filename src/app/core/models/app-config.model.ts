import { LogLevel } from './logging-levels'

export interface IAppConfig {
    version: string,
    serviceUrl: string,
    auth : {
        clientId: string,
        tokenStorageName: string;
      },
      logging : {
        levelMin: string,
        levelMax: string
      }
}
