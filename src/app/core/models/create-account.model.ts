export interface ICreateAccountModel {
    email: string;
    firstname: string;
    lastname: string;
    password: string;
    confirmPassword: string;
    redirectUri?: string;
}