export enum LogLevel {
    Trace = 0,
    Debug = 1,
    Info = 3,
    Error = 4,
    Fatal = 5
}