export interface ISignInModel {
    email: string;
    password: string;
    rememberMe: boolean;
}