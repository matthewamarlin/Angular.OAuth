import { Injectable, Inject, OnDestroy } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { IntervalObservable } from "rxjs/observable/IntervalObservable";

import { ConfigService } from '../services/config.service';
import { IUser } from '../models/user.model'
import { ICreateAccountModel } from '../models/create-account.model'
import { ISignInModel } from '../models/signin.model'
import { JwtHelper } from '../../authHttp/index'
import { Logger } from '../services/logging.service'
import { LogLevel } from '../models/logging-levels'
import { CustomQueryEncoder } from '../../shared/index'

/**
 * An interface for the Authentication Data
 */
export interface IAuthData {
    accessToken: string;
    refreshToken: string;
    rememberMe: boolean;
}

export interface IExternalAuthData {
    externalAccessToken: string,
    userName: string,
    emailAddress: string
    hasLocalAccount: boolean,
    provider: string
}

export interface IResetPasswordData {
    id: string;
    token: string;
    password: string;
    confirmPassword: string;
}

export interface IConfirmAccountModel {
    id: string;
    token: string;
}

/**
 * The AuthService provides functionality for Authentication and Authorization against the WebAPI backend
 */
@Injectable()
export class AuthService {


    constructor(private http: Http,
        private configService: ConfigService,
        private log: Logger,
        private jwtHelper: JwtHelper,
        @Inject('ORIGIN_URL') private origin: string)
        {
            this.log.write(LogLevel.Trace, `${AuthService.name} constructed`);
        }

    /*
        Public Functions
    */
    createAccount(createAccountModel: ICreateAccountModel): Observable<Response> {
        createAccountModel.redirectUri = `${this.origin}/auth/confirm`;

        this.log.write(LogLevel.Debug, `Registering user ${JSON.stringify(createAccountModel)}`)

        return this.http.post(`${this.configService.get.serviceUrl}/api/account/create`, createAccountModel)
            .catch((error: any) => { return Observable.throw(error.json().modelState || 'Server error') });
    }

    signIn(signInModel: ISignInModel): Observable<boolean> {

        let data = `grant_type=password&username=${signInModel.email}&password=${signInModel.password}&client_id=${this.configService.get.auth.clientId}`;
        let requestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }) });

        return this.http.post(this.configService.get.serviceUrl + '/oauth/token', data, requestOptions)
            .do(response => {
                this.log.writeData(LogLevel.Debug, signInModel, "Logged in with data:")

                var responseData = response.json();

                let authData: IAuthData = {
                    accessToken: responseData.access_token,
                    refreshToken: responseData.refresh_token,
                    rememberMe: signInModel.rememberMe
                };

                this.saveAuthData(authData);

                return Observable.of(true);
            }).catch(error => {
                return this.toObservableError(error);
            })
    }

    confirmAccount(confirmAccountModel: IConfirmAccountModel): Observable<boolean> {
        let params: URLSearchParams = new URLSearchParams("", new CustomQueryEncoder());
        params.set("id", confirmAccountModel.id);
        params.set('token', confirmAccountModel.token);

        let requestOptions = new RequestOptions({
            search: params
        });

        return this.http.get(`${this.configService.get.serviceUrl}/api/account/confirm`, requestOptions).map((response) => {
            return response.json();
        }).catch((error: any) => {
            return this.toObservableError(error);
        });
    }

    /**
     * Determines if the user is Authenticated
     */
    isAuthenticated(): Observable<boolean> {
        return this.getAccessToken().map((token) => (token != null) &&
                                                    (token != undefined) &&
                                                    (token != null) &&
                                                    !this.jwtHelper.isTokenExpired(token));
    }

    /**
     * Signs Out the current user by notifying the server and removing the authentication data.
     */
    signOut(): any {
        return Observable.defer(() => {
            this.log.write(LogLevel.Debug, "Signing Out")

            let authData = this.getAuthData();
            if(authData == null) return;

            let requestOptions = new RequestOptions({
                headers: new Headers({'Authorization': `Bearer ${authData.accessToken}`})
            })

            return this.http.get(this.configService.get.serviceUrl + '/api/account/logout', requestOptions)
                            .catch((error) => {
                                this.log.write(LogLevel.Error, "An error occured when signing out the current user.", error)
                                return Observable.empty(); //Supress Errors...
                            })
                            .finally(() => {
                                this.removeAuthData();
                                this.log.write(LogLevel.Debug, "SignOut Complete")
                            });
        });
    }

    forgotPassword(email: string): Observable<string>
    {
        let requestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
        let requestData = { "EmailAddress": email, "RedirectUri": `${this.origin}/auth/reset` }

        return this.http.post(`${this.configService.get.serviceUrl}/api/account/forgotpassword`, requestData, requestOptions).do((response) => {
            return Observable.of(true); //TODO
        }).catch((error) => {
            return this.toObservableError(error)
        });
    }

    resetPassword(resetPasswordData: IResetPasswordData) {
        let requestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });

        return this.http.post(`${this.configService.get.serviceUrl}/api/account/resetpassword`, resetPasswordData, requestOptions).do(() => {
            return Observable.of(true) //TODO
        }).catch((error) => {
            let response = error.json();
            let errorList: string[] = new Array();

            if (response.modelState) {
                for (var key in response.modelState) {
                    if (response.modelState.hasOwnProperty(key)) {
                        errorList.push(response.modelState[key]);
                    }
                }
            }

            throw errorList.length > 0 ? errorList :
                response.errorMessage != null ? response.errorMessage :
                    "Server Error";
        });
    }

    /**
     * Register and SignIn using an external 3rd party authentication providers.
     * Note this should only be called once per specific provider per account.
     * Subsequent logins should be done using externalSignIn
     * @param registerExternalData
     */
    registerExternalSignIn(externalSignInData: IExternalAuthData): Observable<boolean> {
        return this.http.post(this.configService.get.serviceUrl + '/api/account/registerexternal', externalSignInData).do(response => {

            let responseData = response.json();

            let authData: IAuthData = {
                accessToken: responseData.access_token,
                rememberMe: false, //TODO ?
                refreshToken: responseData.refresh_token,
            }

            this.saveAuthData(authData);

            return Observable.of(true);
        }).catch(error => {
            return this.toObservableError(error);
        });
    };

    /**
     * SignIn using an external 3rd party authentication providers
     * @param externalData
     */
    externalSignIn(externalData: IExternalAuthData): Observable<boolean> {

        let data = {
            params: {
                provider: externalData.provider,
                externalAccessToken: externalData.externalAccessToken
            }
        };

        return this.http.get(this.configService.get.serviceUrl + '/api/account/ObtainLocalAccessToken', data).do(response => {
            let responseData = response.json();

            let authData : IAuthData = {
                accessToken: responseData.access_token,
                rememberMe: false, //TODO???
                refreshToken: responseData.refresh_token
            }

            this.saveAuthData(authData);

            return Observable.of(true);
        }).catch(error => {
            return this.toObservableError(error);
        });
    }

    /**
     * Gets an AccessToken from the saved AuthData. If the token is expired it attempts
     * to obtain a new access token using the RefreshToken.
     */
    getAccessToken(): Observable<string> {
        return Observable.defer(() => {

            let authData = this.getAuthData();

            if(authData && authData.accessToken) {
                //Expired Token
                if(this.jwtHelper.isTokenExpired(authData.accessToken))
                {
                    if(authData.refreshToken) {
                        //Get a new Access Token - using the saved refresh token.
                        return this.refreshAccessToken();
                    }
                }
                return Observable.of(authData.accessToken);
            }

            return Observable.of(null);
        });
    }

    //TODO Improve this to get User Profile information
    getFullname(): Observable<string> {
        return this.getAccessToken()
            .map(jwt => this.jwtHelper.decodeToken(jwt))
            .map(decodedToken => decodedToken.FullName);
    }

    /*
        Private Functions
    */

    /**
     * Refresh the AccessToken using a RefreshToken (oAuth RefreshToken Workflow).
     */
    private refreshAccessToken(): Observable<string> {
        return Observable.defer(() => {
            let oldAuthData = this.getAuthData();
            if (oldAuthData && oldAuthData.refreshToken)
            {
                let data = "grant_type=refresh_token&refresh_token=" + oldAuthData.refreshToken + "&client_id=" + this.configService.get.auth.clientId;
                let requestOptions = new RequestOptions({headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'})});

                return this.http.post(this.configService.get.serviceUrl + '/oauth/token', data, requestOptions).do(response => {

                    let responseData = response.json();

                    this.removeAuthData();

                    let newAuthData: IAuthData = {
                        accessToken: responseData.access_token,
                        refreshToken: responseData.refresh_token,
                        rememberMe: oldAuthData.rememberMe,
                    }

                    this.log.writeData(LogLevel.Trace, newAuthData, "AccessToken Refreshed")

                    this.saveAuthData(newAuthData);

                }).map(response => {
                    return response.json().access_token; //return the access token
                }).catch(error => {
                    this.signOut().subscribe()
                    return this.toObservableError(error);
                });
            }

            let message = "No refresh token was present"
            this.log.write(LogLevel.Trace, message)
            return Observable.throw(message);
        });
    }

    private getAuthData(): IAuthData {
        let sessionStorageData = sessionStorage.getItem(this.configService.get.auth.tokenStorageName);
        let localStorageData = localStorage.getItem(this.configService.get.auth.tokenStorageName);

        let authData = sessionStorageData ? JSON.parse(sessionStorageData) :
            localStorageData ? JSON.parse(localStorageData) : null;

        return authData;
    }

    private removeAuthData() {
        this.log.write(LogLevel.Debug, "Removing Authentication Data");
        localStorage.removeItem(this.configService.get.auth.tokenStorageName);
        sessionStorage.removeItem(this.configService.get.auth.tokenStorageName);
    }

    private saveAuthData(authData: IAuthData): void {
        if (authData.rememberMe) {
            this.log.writeData(LogLevel.Debug, authData, "Saving AuthData in localStorage");
            localStorage.setItem(this.configService.get.auth.tokenStorageName, JSON.stringify(authData));
        }
        else {
            this.log.writeData(LogLevel.Debug, authData, "Saving AuthData in sessionStorage");
            sessionStorage.setItem(this.configService.get.auth.tokenStorageName, JSON.stringify(authData));
        }

        this.log.writeData(LogLevel.Debug, this.jwtHelper.decodeToken(authData.accessToken), "Decoded (JWT) AccessToken:")
    }


    // private refreshAccessTokenIn(milliseconds: number): void {
    //     Observable.of(0).delay(milliseconds).subscribe(() => {
    //         this.refreshAccessToken().subscribe();
    //         this.log.write(LogLevel.Trace, "refreshAccessTokenIn Completed")
    //     });
    // }

    // private validateSavedAuthData(){
    //     this.log.write(LogLevel.Debug, "Checking for saved AuthData.")
    //     let authData = this.getAuthData();

    //     if(authData)
    //     {
    //         let expiresIn = this.calculateTokenExpiry(authData.accessToken);
    //         this.log.write(LogLevel.Debug, `Saved AccessToken expires in ${expiresIn}.`)
    //         if(expiresIn <= 0)
    //         {
    //             if(authData.refreshToken)
    //             {
    //                 this.log.write(LogLevel.Debug, `Attempting to renew AccessToken using RefreshingToken`)
    //                 this.refreshAccessToken();
    //             }
    //             else
    //             {
    //                 this.log.write(LogLevel.Debug, `No refresh token was resent, removing stale AuthData`)
    //                 this.removeAuthData();
    //             }
    //         }
    //         else
    //         {
    //             this.refreshAccessTokenIn(expiresIn);
    //         }
    //     }
    //     else{
    //         this.log.write(LogLevel.Debug, "No saved AuthData was found.")
    //     }
    // }


    //TODO Consider Moving?
    /*
        Helpers
    */


    /**
     * Calculates the given JWT AccessToken expiry time in milliseconds
     * @param jwt The JWT access token for which the expiry time shall be calculated
     */
    private calculateTokenExpiry(jwt: string): number {
        let decodedToken = this.jwtHelper.decodeToken(jwt);
        let expiryDate = this.jwtHelper.getTokenExpirationDate(jwt);

        let currentDate = new Date();
        let seconds: number = (expiryDate.getTime() - currentDate.getTime()) / 1000;

        return seconds * 1000;
    }

    /**
     * Helper utility to get the error from a response.
     * @param response
     */
    private toObservableError(response: Response) {
        let error = response.json();
        let modelState = error.modelState;
        if (modelState) {
            let result = this.flattenModelState(modelState)[0]; //Since we will only ever have a single error message
            return Observable.throw(result);
        } else if (error.message) {
            return Observable.throw(error.message);
        } else if(error) {
            return Observable.throw(error.error_description);
        } else {
            return Observable.throw('Internal Server Error')
        }
    }

    /**
     * Flattens a WebApi ModelState JSON object into a flat array where the objects 'values' are kept,
     * but the 'keys' of the object are discarded.
     * @param modelStateObject The WebApi ModelState object to flatten
     */
    private flattenModelState(modelStateObject): string[] {
        let result: string[] = new Array();
        for (let key in modelStateObject) {
            if (modelStateObject.hasOwnProperty(key)) {
                result.push(modelStateObject[key]);
            }
        }
        return result;
    }
}
