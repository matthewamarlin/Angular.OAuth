﻿import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../services/config.service'

/**
 * When SignalR runs it will add functions to the global $ variable
 * that you use to create connections to the hub. However, in this
 * class we won't want to depend on any global variables, so this
 * class provides an abstraction away from using $ directly in here.
 */
export class SignalrWindow extends Window {
    $: any;
}

export enum ConnectionState {
    Connecting = 1,
    Connected = 2,
    Reconnecting = 3,
    Disconnected = 4
}

export interface IChatMessage {
    sender: string;
    body: string;
}

/**
 * ChannelService is a wrapper around the functionality that SignalR
 * provides to expose the ideas of channels and events. With this service
 * you can subscribe to specific channels (or groups in signalr speak) and
 * use observables to react to specific events sent out on those channels.
 */
@Injectable()
export class ChatService {

    /**
     * starting$ is an observable available to know if the signalr
     * connection is ready or not. On a successful connection this
     * stream will emit a value.
     */
    starting$: Observable<any>;

    /**
     * connectionState$ provides the current state of the underlying
     * connection as an observable stream.
     */
    connectionState$: Observable<ConnectionState>;

    /**
     * error$ provides a stream of any error messages that occur on the
     * SignalR connection
     */
    error$: Observable<string>;

    /**
     * messageRecieved$ provides an observable which you can subscribe to in order
     * to recieve message output from the chat hub
     */
    messageReceived$: Observable<IChatMessage>;

    // These are used to feed the public observables
    //
    private connectionStateSubject = new Subject<ConnectionState>();
    private startingSubject = new Subject<any>();
    private errorSubject = new Subject<any>();
    private messageReceived = new Subject<IChatMessage>();

    // These are used to track the internal SignalR state
    //
    private hubConnection: any;
    private hubProxy: any;


    constructor(
        @Inject(SignalrWindow) private window: SignalrWindow,
        configService: ConfigService
    )
    {
        if (this.window.$ === undefined || this.window.$.hubConnection === undefined) {
            throw new Error("The variable '$' or the .hubConnection() function are not defined. Please check that the SignalR scripts have been loaded properly.");
        }

        // Set up Observables
        this.connectionState$ = this.connectionStateSubject.asObservable();
        this.error$ = this.errorSubject.asObservable();
        this.starting$ = this.startingSubject.asObservable();
        this.messageReceived$ = this.messageReceived.asObservable();

        this.hubConnection = this.window.$.hubConnection(configService.get.serviceUrl + '/signalr');
        this.hubProxy = this.hubConnection.createHubProxy('chatHub');

        // Define handlers for the connection state events
        this.hubConnection.stateChanged((state: any) => {
            let newState = ConnectionState.Connecting;

            switch (state.newState) {
                case this.window.$.signalR.connectionState.connecting:
                    newState = ConnectionState.Connecting;
                    break;
                case this.window.$.signalR.connectionState.connected:
                    newState = ConnectionState.Connected;
                    break;
                case this.window.$.signalR.connectionState.reconnecting:
                    newState = ConnectionState.Reconnecting;
                    break;
                case this.window.$.signalR.connectionState.disconnected:
                    newState = ConnectionState.Disconnected;
                    break;
            }

            // Push the new state on our subject
            this.connectionStateSubject.next(newState);
        });

        // Define handlers for any errors
        this.hubConnection.error((error: any) => {

            // Push the error on our subject
            this.errorSubject.next(error);

        });

        this.hubProxy.on('receiveMessage', (sender: string, body: string) => {
            return this.messageReceived.next({ body: body, sender: sender });
        });
    }

    /**
     * Start the SignalR connection. The starting$ stream will emit an
     * event if the connection is established, otherwise it will emit an
     * error.
     */
    start(): void {
        // Now we only want the connection started once, so we have a special
        //  starting$ observable that clients can subscribe to, to know if
        //  if the startup sequence is done.
        //
        // If we just mapped the start() promise to an observable, then any time
        //  a client subscried to it the start sequence would be triggered
        //  again since it's a cold observable.
        this.hubConnection.start()
            .done(() => {
                console.log('Connected');
                this.startingSubject.next();
            })
            .fail((error: any) => {
                this.startingSubject.error(error);
            });
    }

    send(sender: string, message: string): void {
        this.hubProxy.invoke('Send', sender, message);
    }
}
