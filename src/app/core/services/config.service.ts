import { IAppConfig } from '../models/app-config.model'
import { Injectable } from '@angular/core';
import { Http, RequestOptions, RequestOptionsArgs } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import { map } from 'rxjs/operator/map';

@Injectable()
export class ConfigService {

  constructor(private http: Http) {  }

  private config: IAppConfig;

  load(url: string): Observable<any> {
    return this.http.get(url)
    .map((res:any) => res.json())
    .do((json: any) => {
        this.config = json;
    })
    .catch((error)=> {
      throw `Error retrieving configuration from '${url}':\n ${error}`
    })
  }

  get get(): IAppConfig {
    return this.config;
  }
}
