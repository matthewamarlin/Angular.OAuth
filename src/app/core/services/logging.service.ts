import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../services/config.service'
import { LogLevel } from '../models/logging-levels'

export abstract class Logger implements ILogger{

    protected _levelMin: LogLevel;
    protected _levelMax: LogLevel;

    constructor(private configService: ConfigService) {
        this._levelMin = this.stringToLogLevel(configService.get.logging.levelMin);
        this._levelMax = this.stringToLogLevel(configService.get.logging.levelMax);
    }

    abstract write(level: LogLevel, message: string, exception?: string);
    abstract writeData(level: LogLevel, object: Object, message?: string);

    private stringToLogLevel(value: string): LogLevel {
        if(value in LogLevel) {
         return LogLevel[value];
        } else {
          throw `Configuration Error: '${value}' is not a recognised LogLevel, the default value will be used.`
        }
    }
}

@Injectable()
export class ConsoleLogger extends Logger
{
    constructor(configService: ConfigService) {
        super(configService);
    }

    write(level: LogLevel, message: string, exception?: string){
        if(this.shouldLog(level)){
            if(exception) {
                console.log(`${LogLevel[level]}: ${message}\r\n${exception}`)
            } else {
                console.log(`${LogLevel[level]}: ${message}`)
            }
        }
    }

    writeData(level: LogLevel, object: Object, message?: string){
            if(this.shouldLog(level)){
                if(message){
                    console.log(`${LogLevel[level]}: ${message}`)
                }
                console.log(object)
            }
    }

    private shouldLog = (level: LogLevel) => {
        return (level >= this._levelMin && level <= this._levelMax)
    }
}

export interface ILogger {
    write(level: LogLevel, message: string, exception?: string)
    writeData(level: LogLevel, object: Object, message?: string);
}
