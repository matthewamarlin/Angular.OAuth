import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http'
import { Observable } from 'rxjs'
import { ConfigService, Logger, LogLevel } from '../core/index'

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
  constructor(private configService: ConfigService, private http: Http, private log: Logger) { }

  ngOnDestroy(): void {
    window.removeEventListener('resize', this.fixHeight, false);
  }

  ngAfterViewInit(): void {
    this.fixHeight();
    window.addEventListener('resize', this.fixHeight, false);
  }

  ngOnInit(): void { }

  //TODO: Hack to set height of iframe - this is because the AdminLTE template sets the height of the content wrapper via JS
  fixHeight() {
    setTimeout(function () {
      var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
      let headerHeight: number = parseInt(window.getComputedStyle(document.getElementsByClassName("main-header")[0], null).getPropertyValue("height"), 10);
      let footerHeight: number = parseInt(window.getComputedStyle(document.getElementsByClassName("main-footer")[0], null).getPropertyValue("height"), 10);
      let iframe: any = document.getElementById('PowerBIFrame');
      iframe.style.height = `${windowHeight - headerHeight - footerHeight}px`;
    }, 1);
  }
}
