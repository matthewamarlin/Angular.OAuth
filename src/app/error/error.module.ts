import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ErrorRoutes } from './error.routing'
import { Error404Component, Error500Component, Error503Component} from './index';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ErrorRoutes)
  ],
  declarations: [Error404Component, Error500Component, Error503Component]
})

export class ErrorModule { }