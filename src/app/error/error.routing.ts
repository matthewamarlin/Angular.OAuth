import { Routes } from '@angular/router';

import { Error404Component, Error500Component, Error503Component} from './index';

export const ErrorRoutes: Routes = [
  {
    path: '',
    children: [{
      path: '404',
      component: Error404Component
    }, {
      path: '500',
      component: Error500Component
    }, {
      path: '503',
      component: Error503Component
    }]
  }
];
