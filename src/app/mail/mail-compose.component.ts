import { Component, OnInit } from '@angular/core';

//declare let $, jQuery: JQueryStatic;
declare let $, jQuery: any;

@Component({
    selector: 'mail-compose',
    templateUrl: './mail-compose.component.html'
})
export class MailComposeComponent implements OnInit {
    
    ngOnInit(){       
        $(function () {
            //Add text editor
            $("#compose-textarea").wysihtml5();
        });
    }
}
