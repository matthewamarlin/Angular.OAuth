import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MailRoutes } from './mail.routing'
import { MailComposeComponent } from './mail-compose.component'
import { MailInboxComponent } from './mail-inbox.component'
import { MailReadComponent } from './mail-read.component'

@NgModule({
    imports: [ 
        CommonModule, 
        RouterModule.forChild(MailRoutes)
    ],
    declarations: [
        MailComposeComponent,
        MailInboxComponent,
        MailReadComponent
    ]
})

export class MailModule { }