import { Routes } from '@angular/router';

import { MailComposeComponent } from './mail-compose.component'
import { MailInboxComponent } from './mail-inbox.component'
import { MailReadComponent } from './mail-read.component';

export const MailRoutes: Routes = [
  {
    path: '',
    children: [{
      path: '',
      redirectTo: "inbox"
    },{
      path: 'inbox',
      component: MailInboxComponent
    }, {
      path: 'compose',
      component: MailComposeComponent
    }, {
      path: 'read',
      component: MailReadComponent
    }]
  }
];
