import { Component, OnInit, OpaqueToken, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';

import { GeoLocationService } from "../../core/index";

@Component({
    selector: 'google-maps',
    templateUrl: './google-maps.component.html',
    styleUrls: ['./google-maps.component.less']
})
export class GoogleMapsComponent implements OnInit {

    lat: number = -26;
    lng: number = 28;
    zoom: number = 16;

    constructor(private geoLocationService: GeoLocationService) { 

    }

    ngOnInit() {
        this.geoLocationService.getLocation({
            enableHighAccuracy: true,
            maximumAge: 6000,
            timeout: 3000
        }).subscribe((location) => {
            this.lat = location.coords.latitude;
            this.lng = location.coords.longitude;
            swal('',  "We've detected your location!", 'success');
        },
        (error: any) => {
            swal('Oops...',  'Something went wrong when detecting your location!', 'error');
        });
    }
}
