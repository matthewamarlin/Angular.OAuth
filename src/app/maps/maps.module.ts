import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { GeoLocationService } from '../core/index';
import { AgmCoreModule } from '@agm/core';

import { MapsRoutes } from './maps.routing'
import { GoogleMapsComponent } from './google/google-maps.component';

@NgModule({
  imports: [CommonModule, 
            RouterModule.forChild(MapsRoutes), 
            AgmCoreModule.forRoot({apiKey: 'AIzaSyAo0hTiczV-JAWKSZbn3UVI0eQnByXiAbY'})],
  declarations: [GoogleMapsComponent]
})

export class MapsModule { }