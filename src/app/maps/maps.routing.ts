import { Routes } from '@angular/router';

import { GoogleMapsComponent } from './google/google-maps.component';

export const MapsRoutes: Routes = [{
  path: '',
  component: GoogleMapsComponent,
  data: {
    heading: 'Google Maps'
  }
}];
