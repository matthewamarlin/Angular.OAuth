import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../core/index'

@Component({
    selector: 'navheader',
    templateUrl: './navheader.component.html',
    styleUrls: ['./navheader.component.less']
})
export class NavHeaderComponent {

    fullname = "Bob Finch"

    constructor(private authService: AuthService, private router: Router)
    { 
        this.authService.getFullname().subscribe( name => {
            this.fullname = name;
        });
    }

    public logOut(): any {        
        this.authService.signOut().finally(() => {
            this.router.navigate(["/auth/signin"]);
        }).subscribe();
    }
}
