import { Component, Input } from '@angular/core';

@Component({
    selector: 'thr',
    template: 
`<div class="strike">
    <span>
        <ng-content></ng-content>
    </span>
</div>`,
    styleUrls: ['./titled-hr.less']
})
export class TitledHrComponent {
    constructor() { }
}